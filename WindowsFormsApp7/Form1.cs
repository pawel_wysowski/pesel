﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp7
{
    public partial class Form1 : Form
    {

        int[] wagi = new int[] { 9, 7, 3, 1, 9, 7, 3, 1, 9, 7};
        Char[] pesel;
        int[] numery = new int[] { 0,0,0,0,0,0,0,0,0,0 };
        int suma = 0;
        int cyfra_kontrolna;


    public Form1()
        {
            InitializeComponent();

        }

        private void button1_Click(object sender, EventArgs e)
        {

            pesel = textBox1.Text.ToCharArray();


            if (textBox1.Text.Length == 11)
            {
                int parsedValue;
                if (int.TryParse(textBox1.Text, out parsedValue))
                {
                    for (int i = 0; i < pesel.Length - 1; i++)
                    {
                        numery[i] = (int)Char.GetNumericValue(pesel[i]);
                    }



                    for (int i = 0; i < numery.Length; i++)
                    {
                        suma += wagi[i] * numery[i];
                    }

                    cyfra_kontrolna = suma % 10;


                    if (cyfra_kontrolna != (int)Char.GetNumericValue(pesel[10]))
                    {
                        MessageBox.Show("Błędny PESEL");
                        ClearAll();
                    }
                    else
                    {
                        using (StreamWriter sw = new StreamWriter(Path.GetTempPath() + "/pesel.txt"))
                        {

                            sw.WriteLine(textBox1.Text);
                        }
                        MessageBox.Show("PESEL zapisany do pliku w folderze Temp");
                    }

                }
                else MessageBox.Show("PESEL nie posiada liter");
            }
            else MessageBox.Show("PESEL ma niepoprawną ilość znaków");
            ClearAll();

        }

        void ClearAll()
        {
            Array.Clear(numery, 0, numery.Length);
            Array.Clear(pesel, 0, pesel.Length);
            textBox1.Clear();
            cyfra_kontrolna = 0;
        }


    }
}
